from os import name
from . import views
from django.urls import path


urlpatterns = [
    path('', views.contact_view, name="contact_view"),
    path('cont/', views.query_view, name="cont"),
    # path('forms/', views.forms_view, name="forms")
]
