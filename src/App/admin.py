from django.contrib import admin
from App.models import Contact

# Register your models here.

        # Using Decorators in code
@admin.register(Contact)

class ContactAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email', 'phone')

        # Not Using Decorators in code
        
# admin.site.register(Contact, ContactAdmin)
