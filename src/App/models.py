from django.contrib import admin
from django.db import models

# Create your models here.

class Contact(models.Model):
    name       = models.CharField(max_length=15, null=True)
    email      = models.EmailField(max_length=50, null=True)
    phone      = models.BigIntegerField(null=True) 
    desc       = models.TextField(max_length=200,blank=True, null=True)
    feedback   = models.TextField(max_length=200, blank=True, null=True)

    def __str__(self):
        return str(self.name)


# class FormData(models.Model):
#     name       = models.CharField(max_length=15, null=True)
#     email      = models.EmailField(max_length=50)
#     phone      = models.BigIntegerField() 
#     desc       = models.TextField(max_length=200)
#     feedback   = models.TextField(max_length=200, blank=True)

#     def __str__(self):
#         return self.name
