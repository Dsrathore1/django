from os import name
from django.db import models, reset_queries
from django.shortcuts import render, HttpResponse

from App.models import Contact

# Create your views here.


def contact_view(request):
    if request.method == 'POST':
        name            = request.POST.get('name')
        email           = request.POST.get('email')
        phone           = request.POST.get('phone')
        desc            = request.POST.get('desc')
        feedback        = request.POST.get('feedback')

        con = Contact(name=name, email=email, phone=phone, desc=desc, feedback=feedback)

        con.save()
        return HttpResponse("Thanks for contact")

    return render(request, 'base.html')

def query_view(request):
    con = Contact.objects.all()
    print('Your Query is : ', con)
    return render(request, 'query.html', {'cont' : con})

# def forms_view(request):
#     return render (request, 'forms.html')